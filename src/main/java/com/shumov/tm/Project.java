package com.shumov.tm;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Project {

    private static List<Project> list = new ArrayList<>();

    private String id;
    private String name;

    public Project(String name){
        UUID uuid = UUID.randomUUID();
        this.id = uuid.toString();
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public static List<Project> getList() {
        return list;
    }

    public void setName(String name) {
        this.name = name;
    }
}
