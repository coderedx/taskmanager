package com.shumov.tm;

import java.util.Scanner;

public class Controller {

    private Scanner scanner = new Scanner(System.in);

    // Создание нового проекта и задания
    public void createTask() {
        System.out.println("[TASK CREATE]\nENTER TASK NAME:");
        String taskName = scanner.nextLine();
        if (taskName.isEmpty()){
            System.out.println("WRONG TASK NAME!");
            return;
        }
        Task.getList().add(new Task(taskName));
    }

    public void createProject() {
        System.out.println("[PROJECT CREATE]\nENTER PROJECT NAME:");
        String projectName = scanner.nextLine();
        if (projectName.isEmpty()){
            System.out.println("WRONG PROJECT NAME!");
            return;
        }
        Project.getList().add(new Project(projectName));
    }


    // Отображение всех проектов и заданий из списков
    public void showTaskList() {
        System.out.println("[TASK LIST]");
        if(Task.getList().isEmpty()){
            System.out.println("TASK LIST IS EMPTY!");
        }
        for (Task task : Task.getList()) {
            System.out.println("TASK ID: " + task.getId() + " TASK NAME: " + task.getName());
        }
    }

    public void showProjectList() {
        System.out.println("[PROJECT LIST]");
        if(Project.getList().isEmpty()){
            System.out.println("PROJECT LIST IS EMPTY!");
        }
        for (Project project : Project.getList()) {
            System.out.println("PROJECT ID: " + project.getId() + " PROJECT NAME: " + project.getName());
        }
    }


    // Изменить имя по ID
    public void editTaskNameById() {
        System.out.println("[EDIT TASK NAME BY ID]");
        if(Task.getList().isEmpty()){
            System.out.println("TASK LIST IS EMPTY!");
            return;
        }
        System.out.println("ENTER TASK ID:");
        String strTaskId = scanner.nextLine();
        if (strTaskId.isEmpty()){
            System.out.println("WRONG ID!");
            return;
        }
        System.out.println("ENTER NEW TASK NAME:");
        String taskName = scanner.nextLine();
        if (taskName.isEmpty()){
            System.out.println("WRONG TASK NAME!");
            return;
        }
        for (Task task : Task.getList()) {
            if (strTaskId.equals(task.getId())) {
                task.setName(taskName);
                System.out.println("TASK NAME HAS BEEN CHANGED SUCCESSFULLY");
                return;
            }
        }
        System.out.println("TASK WITH THIS ID DOES NOT EXIST!");
    }



    public void editProjectNameById() {
        System.out.println("[EDIT PROJECT NAME BY ID]");
        if(Project.getList().isEmpty()){
            System.out.println("PROJECT LIST IS EMPTY!");
            return;
        }
        System.out.println("ENTER PROJECT ID:");
        String strProjectId = scanner.nextLine();
        if (strProjectId.isEmpty()){
            System.out.println("WRONG ID!");
            return;
        }
        System.out.println("ENTER NEW PROJECT NAME:");
        String projectName = scanner.nextLine();
        if (projectName.isEmpty()){
            System.out.println("WRONG PROJECT NAME!");
            return;
        }
        for (Project project : Project.getList()) {
            if (strProjectId.equals(project.getId())) {
                project.setName(projectName);
                System.out.println("PROJECT NAME HAS BEEN CHANGED SUCCESSFULLY");
                return;
            }
        }
        System.out.println("PROJECT WITH THIS ID DOES NOT EXIST!");

    }


    // Удаление по ID
    public void removeTaskById() {
        System.out.println("[REMOVE TASK BY ID]");
        if(Task.getList().isEmpty()){
            System.out.println("TASK LIST IS EMPTY!");
            return;
        }
        System.out.println("ENTER TASK ID:");
        String strTaskId = scanner.nextLine();
        if(strTaskId.isEmpty()){
            System.out.println("WRONG ID!");
            return;
        }
        for (Task task : Task.getList()) {
            if (strTaskId.equals(task.getId())) {
                Task.getList().remove(task);
                System.out.println("TASK HAS BEEN REMOVED SUCCESSFULLY");
                return;
            }
        }
        System.out.println("TASK WITH THIS ID DOES NOT EXIST!");
    }



    public void removeProjectById() {
        System.out.println("[REMOVE PROJECT BY ID]");
        if(Project.getList().isEmpty()){
            System.out.println("PROJECT LIST IS EMPTY!");
            return;
        }
        System.out.println("ENTER PROJECT ID:");
        String strProjectId = scanner.nextLine();
        if (strProjectId.isEmpty()){
            System.out.println("WRONG ID!");
            return;
        }
        for (Project project : Project.getList()) {
            if (strProjectId.equals(project.getId())) {
                Project.getList().remove(project);
                System.out.println("PROJECT HAS BEEN REMOVED SUCCESSFULLY");
                return;
            }
        }
        System.out.println("PROJECT WITH THIS ID DOES NOT EXIST!");
    }

    // help
    public void help() {
        getAllCommands();
    }

    public void exit() {
        System.exit(0);
    }

    // Выполнение
    public void execute() {
        System.out.println("WELCOME TO TASK MANAGER\n" +
                "ENTER \"help\" TO GET COMMAND LIST");
        while (true) {
            System.out.println("\nENTER COMMAND:");
            Command command = Command.getCommands().get(scanner.nextLine().toLowerCase());
            commandSwitchStep1(command);
            commandSwitchStep2(command);
        }
    }

    //Список команд
    private void getAllCommands() {
        for (Command command : Command.values()) {
            System.out.println(command.getCommandText()+": "+command.getDescription());
        }
    }

    private void commandSwitchStep1(Command command) {
        if (command != null) {
            switch (command) {
                case HELP:
                    this.help();
                    break;
                case EXIT:
                    this.exit();
                    break;
                case CREATE_TASK:
                    this.createTask();
                    break;
                case CREATE_PROJECT:
                    this.createProject();
                    break;
                case REMOVE_TASK_BY_ID:
                    this.removeTaskById();
                    break;
                case REMOVE_PROJECT_BY_ID:
                    this.removeProjectById();
                    break;
            }
        }
    }

    private void commandSwitchStep2(Command command) {
        if (command != null) {
            switch (command) {
                case EDIT_TASK_NAME_BY_ID:
                    this.editTaskNameById();
                    break;
                case EDIT_PROJECT_NAME_BY_ID:
                    this.editProjectNameById();
                    break;
                case TASK_LIST:
                    this.showTaskList();
                    break;
                case PROJECT_LIST:
                    this.showProjectList();
                    break;
            }
        } else {
            System.out.println("WRONG COMMAND!");
        }
    }
}